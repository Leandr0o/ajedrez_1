package Piezas;

import java.util.Map;

public class caballo extends pieza {
    public caballo(Map<String, String> args){
        importar(args);
    }
    //(f1-f2)*(f1-f2) +(c1-c2)*(c1-c2) == 5
    public String queSoy(){

        return("Soy un caballo");
    }
    public String mover(int x,int y){
        int deltaX = Math.abs((x-this.posicion.get(0))*(x-this.posicion.get(0)));
        int deltaY = Math.abs((y-this.posicion.get(1))*(y-this.posicion.get(1)));
        int deltaPos = deltaX+deltaY;
        if(deltaPos==3) {
            return "movimiento_realizado";
        }
        return "movimiento_rechazado";
    }

}
