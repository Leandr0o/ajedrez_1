package Piezas;

import java.util.Map;

public class rey extends pieza {
    public rey(Map<String, String> args) {
        importar(args);
    }{}

    public String mover(int x,int y){
        int deltaX = Math.abs(x-this.posicion.get(0));
        int deltaY = Math.abs(y-this.posicion.get(1));
        if ((deltaX<2)&&(deltaY<2)){
            return "movimiento_realizado";
        }
        return "movimiento_rechazado";
    }
}
